import xml.etree.ElementTree as ET
import argparse
import os
import logging
import re
#from tqdm import tqdm

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def parse_list_file(file_path):
    logging.info(f"Reading list file: {file_path}")
    with open(file_path, 'r', encoding='utf-8') as file:
        return [line.strip() for line in file.readlines() if line.strip()]

def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Filter and modify an XML file according to specified criteria."
    )
    parser.add_argument('-input', required=True, help="Path to the input XML file")
    parser.add_argument('-output', required=True, help="Base path for the output XML files (version will be appended automatically)")
    parser.add_argument('-sourcefile_include', nargs='*', help="Include sourcefiles (can be files or text strings)")
    parser.add_argument('-sourcefile_exclude', nargs='*', help="Exclude sourcefiles (can be files or text strings)")
    parser.add_argument('-machine_include', nargs='*', help="Include machines by name (can be files or text strings)")
    parser.add_argument('-machine_exclude', nargs='*', help="Exclude machines by name (can be files or text strings)")
    parser.add_argument('-romof_include', nargs='*', help="Include machines by romof (can be files or text strings)")
    parser.add_argument('-romof_exclude', nargs='*', help="Exclude machines by romof (can be files or text strings)")
    parser.add_argument('-add_rom_name', nargs=2, action='append', metavar=('ROM_NAME', 'MACHINE_NAMES'), help="Add rom_name to specified machines. Example: -add_rom_name 'epr-19338a.bin' 'dirtdvls,eca'")
    parser.add_argument('-only-tate', action='store_true', help="Output only machines with display rotate = 270")
    return parser.parse_args()

def collect_criteria(criteria_list):
    result = set()
    if criteria_list:
        for item in criteria_list:
            if os.path.isfile(item):
                result.update(parse_list_file(item))
            else:
                result.add(item)
    return result

def filter_machines(root, sourcefile_include, sourcefile_exclude, machine_include, machine_exclude, romof_include, romof_exclude):
    logging.info("Filtering machines...")
    machines_to_remove = []
    for machine in root.findall('machine'):
        sourcefile = machine.get('sourcefile')
        name = machine.get('name')
        romof = machine.get('romof')
        
        # Filter by sourcefile
        if sourcefile_include and (not sourcefile or sourcefile not in sourcefile_include):
            machines_to_remove.append(machine)
            continue
        if sourcefile_exclude and (sourcefile in sourcefile_exclude):
            machines_to_remove.append(machine)
            continue

        # Filter by machine name
        if machine_include and (not name or name not in machine_include):
            machines_to_remove.append(machine)
            continue
        if machine_exclude and name in machine_exclude:
            machines_to_remove.append(machine)
            continue

        # Filter by romof
        if romof_include and (not romof or romof not in romof_include):
            machines_to_remove.append(machine)
            continue
        if romof_exclude and romof in romof_exclude:
            machines_to_remove.append(machine)
            continue
        
    for machine in machines_to_remove:
        root.remove(machine)
    logging.info(f"Filtered out {len(machines_to_remove)} machines")

def exclude_non_referenced_devices(root):
    logging.info("Excluding non-referenced devices...")
    referenced_devices = set()
    for machine in root.findall('machine'):
        for device_ref in machine.findall('device_ref'):
            referenced_devices.add(device_ref.get('name'))

    devices_to_remove = []
    for machine in root.findall('machine'):
        if machine.get('isdevice') == 'yes' and machine.get('name') not in referenced_devices:
            devices_to_remove.append(machine)

    for device in devices_to_remove:
        root.remove(device)
    logging.info(f"Excluded {len(devices_to_remove)} non-referenced devices")

def add_rom_names(root, rom_name_machines):
    logging.info("Adding ROM names to specified machines...")
    rom_elements = {}  # Dictionary to store rom elements by their name

    # Collect all rom elements in the source XML and log them
    for machine in root.findall('machine'):
        for rom in machine.findall('rom'):
            rom_name = rom.get('name')
            if rom_name:
                rom_name = rom_name.strip()  # Ensure there are no leading/trailing spaces
                if rom_name not in rom_elements:
                    rom_elements[rom_name] = rom
                    logging.debug(f"Found ROM in XML: {rom_name}")
                else:
                    logging.debug(f"Duplicate ROM name found: {rom_name}")

    for rom_name, machine_names in rom_name_machines:
        machine_list = machine_names.split(',')
        rom_names = [rn.strip() for rn in rom_name.split(',')]

        for machine in root.findall('machine'):
            if machine.get('name') in machine_list:
                last_rom_index = None
                for idx, element in enumerate(machine):
                    if element.tag == 'rom':
                        last_rom_index = idx

                for rn in rom_names:
                    if rn in rom_elements:
                        # Create a new rom element
                        new_rom_element = ET.Element("rom", rom_elements[rn].attrib)
                        logging.info(f"ROM {rn} found and added to machine {machine.get('name')}")
                    else:
                        # If the ROM is not found in the source, create a ROM element with just the name attribute
                        new_rom_element = ET.Element("rom")
                        new_rom_element.set("name", rn)
                        logging.warning(f"ROM {rn} not found in source XML, adding with name only.")

                    if last_rom_index is None:
                        machine.insert(0, new_rom_element)
                    else:
                        machine.insert(last_rom_index + 1, new_rom_element)
                        last_rom_index += 1
                    logging.info(f"Added ROM {rn} to machine {machine.get('name')}")

    # Log all ROM names found in the XML
    logging.info("List of all ROM names found in the XML:")
    for rom_name in rom_elements.keys():
        logging.info(rom_name)

def create_no_clones_xml(root, no_clones_output, header):
        logging.info("Creating no-clones XML file...")
        no_clones_root = ET.Element(root.tag, root.attrib)  # Use attributes from the original root
        for machine in root.findall('machine'):
            if not machine.get('cloneof'):
                no_clones_root.append(machine)
        
        no_clones_tree = ET.ElementTree(no_clones_root)
        with open(no_clones_output, 'w', encoding='utf-8') as f:
            f.write(header)
            f.write(ET.tostring(no_clones_root, encoding='unicode', method='xml'))
        logging.info(f"No-clones XML file created: {no_clones_output}")

def write_xml_with_header(tree, output_file, header):
    logging.info(f"Writing XML file with header: {output_file}")
    xml_str = ET.tostring(tree.getroot(), encoding='unicode')
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(header)
        f.write(xml_str)

def log_summary(root):
    total_machines = len(root.findall('machine'))
    total_isdevice = sum(1 for machine in root.findall('machine') if machine.get('isdevice') == 'yes')
    total_isbios = sum(1 for machine in root.findall('machine') if machine.get('isbios') == 'yes')
    total_ismechanical = sum(1 for machine in root.findall('machine') if machine.get('ismechanical') == 'yes')

    total_driver_imperfect = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('status') == 'imperfect')
    total_driver_emulation_good = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('emulation') == 'good')
    total_driver_savestate_unsupported = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('savestate') == 'unsupported')
    total_driver_requiresartwork_no = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('requiresartwork') == 'no')
    total_driver_unofficial_no = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('unofficial') == 'no')
    total_driver_nosoundhardware_no = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('nosoundhardware') == 'no')
    total_driver_incomplete_no = sum(1 for machine in root.findall('machine') if machine.find('driver') is not None and machine.find('driver').get('incomplete') == 'no')

    total_feature_sound_imperfect = sum(1 for machine in root.findall('machine') if any(feature.get('type') == 'sound' and feature.get('status') == 'imperfect' for feature in machine.findall('feature')))

    logging.info(f"Summary of the output XML:")
    logging.info(f"Total machines: {total_machines}")
    logging.info(f"Total isdevice machines: {total_isdevice}")
    logging.info(f"Total isbios machines: {total_isbios}")
    logging.info(f"Total ismechanical machines: {total_ismechanical}")

    logging.info(f"Total machines with driver status 'imperfect': {total_driver_imperfect}")
    logging.info(f"Total machines with driver emulation 'good': {total_driver_emulation_good}")
    logging.info(f"Total machines with driver savestate 'unsupported': {total_driver_savestate_unsupported}")
    logging.info(f"Total machines with driver requiresartwork 'no': {total_driver_requiresartwork_no}")
    logging.info(f"Total machines with driver unofficial 'no': {total_driver_unofficial_no}")
    logging.info(f"Total machines with driver nosoundhardware 'no': {total_driver_nosoundhardware_no}")
    logging.info(f"Total machines with driver incomplete 'no': {total_driver_incomplete_no}")

    logging.info(f"Total machines with feature type 'sound' and status 'imperfect': {total_feature_sound_imperfect}")

def extract_version_from_header(header):
    match = re.search(r'build="([^"\(]+)', header)
    return match.group(1).strip() if match else "unknown"

def remove_mechanical_machines(root):
    logging.info("Removing mechanical machines...")
    mechanical_machines = [machine for machine in root.findall('machine') if machine.get('ismechanical') == 'yes']
    for machine in mechanical_machines:
        root.remove(machine)
    logging.info(f"Removed {len(mechanical_machines)} mechanical machines")

def filter_tate_machines(root):
    logging.info("Filtering TATE machines (rotate = 270)...")
    tate_machines = [machine for machine in root.findall('machine') if any(display.get('rotate') == '270' for display in machine.findall('display'))]
    tate_root = ET.Element('mame')
    for machine in tate_machines:
        tate_root.append(machine)
    return tate_root

def main():
    args = parse_arguments()

    logging.info(f"Loading XML file: {args.input}")
    tree = ET.parse(args.input)
    
    # Read the header part of the input XML file
    with open(args.input, 'r', encoding='utf-8') as f:
        header_lines = []
        for line in f:
            header_lines.append(line)
            if line.strip().startswith('<mame'):
                # Extract the build version from this line
                match = re.search(r'build="([^"]*)"', line)
                if match:
                    build_version = match.group(1)
                break
        # Correct the header by ensuring only one <mame> tag using the extracted version
        header = ''.join(header_lines[:-1])  # Remove the last line which might be a duplicate <mame> tag
        header += f'<mame build="{build_version}" debug="no" mameconfig="10">\n'

    # Ensure there's only one root element
    root = tree.getroot()
    while root.tag != 'mame':
        root = root[0]  # go one level deeper until we find 'mame' tag

    logging.info(f"XML file loaded: {args.input}")

    sourcefile_include = collect_criteria(args.sourcefile_include)
    sourcefile_exclude = collect_criteria(args.sourcefile_exclude)
    machine_include = collect_criteria(args.machine_include)
    machine_exclude = collect_criteria(args.machine_exclude)
    romof_include = collect_criteria(args.romof_include)
    romof_exclude = collect_criteria(args.romof_exclude)

    filter_machines(root, sourcefile_include, sourcefile_exclude, machine_include, machine_exclude, romof_include, romof_exclude)
    remove_mechanical_machines(root)
    exclude_non_referenced_devices(root)

    if args.add_rom_name:
        add_rom_names(root, args.add_rom_name)

    version = extract_version_from_header(header)
    output_file = f"{args.output}-{version}-pixl.xml"
    no_clones_output = f"{args.output}-{version}-no-clones-pixl.xml"
    
    write_xml_with_header(tree, output_file, header)
    create_no_clones_xml(root, no_clones_output, header)
    
    log_summary(root)
    
    if args.only_tate:
        logging.info("Creating TATE-only XML files...")
        tate_root = filter_tate_machines(root)
        
        tate_output_file = f"{args.output}-{version}-tate-pixl.xml"
        tate_tree = ET.ElementTree(tate_root)
        write_xml_with_header(tate_tree, tate_output_file, header)

        tate_no_clones_root = ET.Element(root.tag, root.attrib)  # Use attributes from the original root
        for machine in tate_root.findall('machine'):
            if not machine.get('cloneof'):
                tate_no_clones_root.append(machine)
        
        tate_no_clones_output_file = f"{args.output}-{version}-tate-no-clones-pixl.xml"
        tate_no_clones_tree = ET.ElementTree(tate_no_clones_root)
        write_xml_with_header(tate_no_clones_tree, tate_no_clones_output_file, header)

        logging.info(f"TATE-only XML file created: {tate_output_file}")
        logging.info(f"TATE-only no-clones XML file created: {tate_no_clones_output_file}")

if __name__ == "__main__":
    main()
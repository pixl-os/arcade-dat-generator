# check System
# Declare an array of string with type
declare -a Systems=("Mame" "Atomiswave" "Naomi" "NaomiGd" "Naomi2" "Neogeo" "Model3" "Model2" "Sys2x6" "Lindbergh") # "STV" "Triforce"

# change directory for execute datutil in buildroot
# Ugly check for commit sha or branche
ARCADE_DAT_GENERATOR_VERSION=$(grep "ARCADE_DAT_GENERATOR_VERSION =" "../package/arcade-dat-generator/arcade-dat-generator.mk" |awk -F'= ' '{print $2}')
cd "../output/build/arcade-dat-generator-$ARCADE_DAT_GENERATOR_VERSION/"

# check version of mame romset
Mame_Version=$(ls mame*.xml | awk -F"e" '{print $2}' | awk -F. '{print $1}')

# dat source in MameDev
# https://github.com/mamedev/mame/releases/download/mame0240/mame0240lx.zip
# need use "mameXXXX.xml" for create all dat to pixL
Dat_Input="mame${Mame_Version}.xml"

Dat_Output_Path='Dat_pixL_Arcade' # -o option
list_Filter_Path='DatUtils-pixL'

# create log dir if not exist
mkdir -p log/
# create output folder 
mkdir -p "${Dat_Output_Path}"/

# Mame
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_exclude "${list_Filter_Path}"/"${Systems[0]}"/SourceFiles_Removed \
	-machine_exclude "${list_Filter_Path}"/"${Systems[0]}"/RomOf_Removed \
	-machine_include "vf3" \
	-output "${Dat_Output_Path}"/"${Systems[0]}"

# Mame only Tate games
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_exclude "${list_Filter_Path}"/"${Systems[0]}"/SourceFiles_Removed \
	-machine_exclude "${list_Filter_Path}"/"${Systems[0]}"/RomOf_Removed \
	-only-tate \
	-output "${Dat_Output_Path}"/"${Systems[0]}"

# Atomiswave
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'sega/dc_atomiswave.cpp' \
	-machine_exclude "awbios" \
	-output "${Dat_Output_Path}"/"${Systems[1]}"

# Naomi
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'sega/naomi.cpp' \
	-romof_include 'naomi' \
	-machine_exclude "${list_Filter_Path}"/"${Systems[2]}"/RomOf_Removed \
	-output "${Dat_Output_Path}"/"${Systems[2]}"

# NaomiGD
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'sega/naomi.cpp' \
	-machine_exclude "${list_Filter_Path}"/"${Systems[3]}"/RomOf_Removed \
	-romof_include 'naomigd' \
	-output "${Dat_Output_Path}"/"${Systems[3]}"

# Naomi2
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'sega/naomi.cpp' \
	-romof_include 'naomi2' \
	-machine_exclude "${list_Filter_Path}"/"${Systems[4]}"/RomOf_Removed \
	-output "${Dat_Output_Path}"/"${Systems[4]}"

# Neogeo
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'neogeo/neogeo.cpp' \
	-machine_exclude "${list_Filter_Path}"/"${Systems[5]}"/RomOf_Removed \
	-output "${Dat_Output_Path}"/"${Systems[5]}"

# Model3
python3 filter_mame_xml.py \
	-input "${Dat_Input}" \
	-sourcefile_include 'sega/model3.cpp' \
	-machine_exclude "${list_Filter_Path}"/"${Systems[6]}"/RomOf_Removed \
	-output "${Dat_Output_Path}"/"${Systems[6]}"
	# -add_rom_name "epr-19338a.bin" "eca,ecaj,ecap,ecau" \
	# -add_rom_name "epr-18261.ic9" "lemans24" \
	# -add_rom_name "epr-18022.ic2" "fvipers2,fvipers2o,vf3,vf3a,vf3c,vf3tb,von2,von2a,von2o,von254g,vs2,vs215,vs215o,vs298,vs29815,vs299,vs29915,vs29915a,vs29915j,vs299a,vs299j,vs2v991" \
